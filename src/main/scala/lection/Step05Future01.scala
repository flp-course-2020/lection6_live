package lection
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object Step05Future01 extends App {
  /*
   Предыдущие решения имеют свои недостатки. Есть ли в Scala инструмент
   позволяющий легко и непринуждённо описывать асинхронные вычисления?
   Да, есть, и имя ему Future
   */

  def loop(program: => Unit): Nothing = {
    program
    loop(program)
  }

  def smthRequest(str: String): String = {
    Thread.sleep(5000)
    str.reverse
  }

  loop {
    print("==> ")
    val userInput = io.StdIn.readLine()

    if (userInput == "exit") {
      println("Goodbye!")
      sys.exit(0)
    }

    Future {
      val response = smthRequest(userInput)
      println(response)
    }
  }
}
