package lection

object Step04Async1 extends App {

  /*
    Асинхронное программирование - не блокируем основной поток выполнения,
    делегируем исполнение участков кода другим потокам
   */

  def loop(program: => Unit): Nothing = {
    program
    loop(program)
  }

  def smthRequest(str: String): String = {
    Thread.sleep(5000)
    str.reverse
  }

  def executeAsync(program: => Unit): Unit = {
    val th = new Thread {
      override def run(): Unit = {
        program
      }
    }
    th.start()
  }

  loop {
    print("==> ")
    val userInput = io.StdIn.readLine()

    if (userInput == "exit") {
      println("Goodbye!")
      sys.exit(0)
    }

    executeAsync {
      val response = smthRequest(userInput)
      println(response)
    }
  }
}
