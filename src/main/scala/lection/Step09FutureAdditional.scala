package lection

import scala.concurrent.Future

object Step09FutureAdditional extends App {

  val successFuture = Future.successful(2)

  val failedFuture = Future.failed(new Exception("smth exception"))

}
