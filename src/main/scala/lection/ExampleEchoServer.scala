package lection

object ExampleEchoServer extends App {
  import java.net._
  import java.io._
  import scala.io._

  val server = new ServerSocket(8888)

  def loop(program: => Unit): Nothing = {
    program
    loop(program)
  }

  loop {
    val s = server.accept()
    val out = new PrintStream(s.getOutputStream)
    def help: Unit = {
      new BufferedSource(s.getInputStream).getLines().next() match {
        case x if x.toLowerCase() == "exit" => ()
        case x =>
          out.println(x)
          help
      }
    }
    help
    out.flush()
    s.close()
  }

}
