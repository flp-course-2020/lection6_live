package lection

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

object Step05Future04 extends App {

  val f = Future {
    println("start future")
    Thread.sleep(2000)
    2 * 2
  }

  val fWithCallback = f.foreach(println)

  println("Ждем, пока выполнится фьюча")
  Await.ready(f, Duration.Inf)
  println("Та-да!")
}

