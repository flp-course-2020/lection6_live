package lection

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object Step05Future07 extends App {

  def myFuture(x: Int): Future[String] = Future {
    println("Start myFuture")
    Thread.sleep(2000)
    (x * x).toString
  }

  val f = Future {
    println("Start future")
    Thread.sleep(2000)
    42
  }

  val resultF = f.flatMap(myFuture)

  f.foreach(println)
  resultF.foreach(println)

  println("Ждем наши фьючи")
  Thread.sleep(4100)

}
