package lection

object Step03Sync extends App {
  /*
   Синхронное исполнение программы - честно ждём выполнения каждой инструкции
   */

  def loop(program: => Unit): Nothing = {
    program
    loop(program)
  }

  def smthRequest(str: String): String = {
    Thread.sleep(1000)
    str.reverse
  }


  loop {
    print("==> ")
    val userInput = io.StdIn.readLine()

    if (userInput == "exit") {
      println("Goodbye!")
      sys.exit(0)
    }

    val response = smthRequest(userInput)

    println(response)
  }

}
