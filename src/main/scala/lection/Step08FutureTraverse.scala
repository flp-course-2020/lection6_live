package lection

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.Random

object Step08FutureTraverse extends App {
  val r = new Random(42)

  //Придумать явный пример, почему с этим неудобно работать

  val stubList = List.fill(10)(r.nextInt(100))

  val listOfFutures = stubList.map {x =>
    Future {
      Thread.sleep(1000)
      x * x
    }
  }

  val result = Future.sequence(listOfFutures)

  Await.ready(result, Duration.Inf)
  println(result)

  def getAnyFuture(x: Int): Future[String] = Future {
    Thread.sleep(1000)
    x.toString
  }

  val a = Future.traverse(stubList)(getAnyFuture)

}
