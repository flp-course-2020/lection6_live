package lection

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration._

object Step05Future03 extends App {
  /*
   Есть ли какой-то способ выдернуть значение из Future?
   Есть.
   */

  val f = Future {
    println("start future")
    Thread.sleep(5000)
    2 * 2
  }

  val result = Await.result(f, Duration.Inf)

  println(result)

}
