package lection

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object Step07FutureForSyntax {
  def f() = Future {
    2 * 2
  }

  def f2() = Future {
    4 * 4
  }

  def f3(x: Int): Future[Int] = Future {
    x * x
  }

  val result = for {
    x1 <- f()
    x2 <- f2()
    x3 <- f3(x2 + x1)
  } yield x3
}
