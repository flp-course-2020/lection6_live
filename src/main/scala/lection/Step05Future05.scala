package lection

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

object Step05Future05 extends App {

  val f = Future {
    println("start future")
    Thread.sleep(2000)
    2 * 2
  }

  val fWithHiddenCallback = f.andThen {
    case Success(value) => println(value)
    case Failure(exception) => println(exception.getMessage)
  }

  println("Ждем фьючу")
  Await.ready(f, Duration.Inf)
  val result = Await.result(fWithHiddenCallback, Duration.Inf)

  println(s"Result: $result")

}
