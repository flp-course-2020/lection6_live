package lection

object Step04Async2 extends App {
  // В прошлом решении мы каждый раз создавали новый тред.
  // Создание треда операция не из дешёвых, поэтому
  // создадим свой пулл потоков, который будет выполнять наши методы

  def loop(program: => Unit): Nothing = {
    program
    loop(program)
  }

  def smthRequest(str: String): String = {
    Thread.sleep(5000)
    str.reverse
  }

  object Async {
    private val queue = scala.collection.mutable.Queue[() => Unit]()

    private val pool = for (i <- 1 to Runtime.getRuntime.availableProcessors()) yield {
      def deque: Option[() => Unit] = queue.synchronized {
        if (queue.isEmpty) None
        else Some(queue.dequeue())
      }
      val th = new Thread(s"My thread $i") {
        override def run(): Unit = loop {
          deque match {
            case Some(program) => program()
            case None =>
          }
        }
      }
      th.start()
      th
    }

    def executeAsync(program: => Unit): Unit = {
      queue.enqueue(() => program)
    }
  }

  loop {
    print("==> ")
    val userInput = io.StdIn.readLine()

    if (userInput == "exit") {
      println("Goodbye!")
      sys.exit(0)
    }
    Async.executeAsync {
      val response = smthRequest(userInput)
      println(response)
    }
  }
}
