package lection

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

object Step06FutureError01 extends App {

  def futureWithPotentialError(x: Int): Future[Int] = Future {
    if (x % 2 == 0) x * x
    else throw new Exception("I hate odd numbers")
  }

  val correct = futureWithPotentialError(4)

  correct.foreach(println)

  println("Ждем фьючу")
  Await.ready(correct, Duration.Inf)

  try {
    val exception = futureWithPotentialError(3)
  }
  Thread.sleep(100)

//  println(exception)
}
