package lection

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

object Step05Future06 extends App {

  val f = Future {
    println("Start future")
    Thread.sleep(2000)
    2 * 2
  }

  val sqrFuture = f.map { x =>
    Thread.sleep(2000)
    x * x
  }

  f.foreach(println)

  sqrFuture.foreach(println)

  println("Подождём наши фьючи")
  Await.ready(sqrFuture, Duration.Inf)

}
