package lection

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

object Step06FutureError02 extends App {

  def futureWithPotentialError(x: Int): Future[Int] = Future {
    if (x % 2 == 0) x * x
    else throw new Exception("I hate odd numbers")
  }

  val f = futureWithPotentialError(3)

  val recoveredFuture = f.recover {
    case _: Exception => 3 * 3
  }

  println(f)
  recoveredFuture.foreach(println)

  println("Ждём нашу фьючу")
  Await.ready(recoveredFuture, Duration.Inf)


}
