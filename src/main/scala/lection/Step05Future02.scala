package lection

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Step05Future02 extends App {

  /*
   Future - это тип, который как бы описывает, что значение появится в будущем.
   Если обратиться к нему, то этого значения, на момент обращения может не оказаться
   */


  val f = Future {
    println("start future")
    Thread.sleep(5000)
    2 * 2
  }

  println(f)

  Thread.sleep(2500)

  println(f)

  Thread.sleep(2505)

  println(f)

  val f2 = Future {
    2 * 2
  }

}
