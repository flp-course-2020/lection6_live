package lection

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object ExampleEchoServerImprove extends App {
  import java.io._
  import java.net._

  import scala.io._

  val server = new ServerSocket(8888)

  def loop(program: => Unit): Nothing = {
    program
    loop(program)
  }


  loop {
    val s = server.accept()
    val out = new PrintStream(s.getOutputStream)
    def help: Unit = {
      new BufferedSource(s.getInputStream).getLines().next() match {
        case x if x.toLowerCase() == "exit" => ()
        case x =>
          out.println(x)
          help
      }
    }
    Future {
      help
      out.flush()
      s.close()
    }
  }

}
