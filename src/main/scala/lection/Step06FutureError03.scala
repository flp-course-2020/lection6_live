package lection

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

object Step06FutureError03 extends App {

  object MyFavouriteException extends Exception("text text text")

  def futureWithPotentialError(x: Int): Future[Int] = Future {
    if (x % 2 == 0) x * x
    else throw new Exception("I hate odd numbers")
  }

  val f = futureWithPotentialError(3)

  val fWithOtherException = f.transform(identity, {
    case _: Exception => MyFavouriteException
  })

  println("Ждем фьючу")
  Await.ready(fWithOtherException, Duration.Inf)

  println(f)
  println(fWithOtherException)

}
